var Parse = require('parse/node');
Parse.initialize("K1oKmCPGE0kVSSCE3BWFQwsbqtP3xxu5CZixPrUQ", "lZzvKPLInrCqZlu235AQCq38SIGsKFPA9XZC0Ihy");




//NOTE TO SELF:
//Instead of entirely re-creating the query for each search, pre-make each one
//as much as you can, and just add the input criteria when the function
//is called. It'll save time.





// Initialize express
var express = require('express');
var app = express();

        //*******************************************************
        //***************************************************down
var unique_num = 0;

//Create objects for Ads and for User
// var Ad = Parse.Object.extend("Ad");
var AdTicket = Parse.Object.extend("AdTicket");
var AdLease = Parse.Object.extend("AdLease");
var AdBook = Parse.Object.extend("AdBook");
var User = Parse.Object.extend("User");
var Bid = Parse.Object.extend("Bid");

        //*****************************************************up
        //*******************************************************

var server = app.listen(8080, function () {
  var host = server.address().address;
  var port = server.address().port;

    console.log('app listening at http://%s:%s', host, port);
});

var io = require('socket.io')(server);

io.on('connection', function (socket) {

        //*******************************************************
        //***************************************************down

        socket.on('request_ticket', function () {

                var queryTicket = new Parse.Query(AdTicket);  //Creates a query for the Ad class
                queryTicket.containedIn("Type", ["Ticket"]);
                queryTicket.containedIn("Available", ["Yes"]);
                // queryTicket.lessThanOrEqualTo("Price", price);
                // queryTicket.containedIn("Event", ent);
                queryTicket.ascending("Price");


                queryTicket.find({
                         success: function(results){  //saves the search to results
                                socket.emit('receive_tickets', results);
                                }
                        });
        });

                //Create the ad object from the given information
        socket.on('create_ticket', function (name, event_date, type, price, seat, contact, posted_date) {
                var ad = new AdTicket();
                ad.set("Event", name);
                ad.set("EventDate", event_date);
                ad.set("Price", price);
                ad.set("Seat", seat);
                ad.set("Available", "Yes");
                ad.set("Seller", contact);
                ad.set("ID", unique_num);
                ad.set("Type", "Ticket");
                ad.set("TicketType", type);
                ad.set("PostedDate", posted_date);
                unique_num++;
                ad.save();
        })

        socket.on('request_book', function () {

                var queryBook = new Parse.Query(AdBook);  //Creates a query for the Ad class
                queryBook.containedIn("Type", ["Book"]);
                queryBook.containedIn("Available", ["Yes"]);
                //queryBook.equalTo("ISBN", isbn);
                queryBook.ascending("Price");


                queryBook.find({
                         success: function(results){  //saves the search to results
                                socket.emit('receive_books', results);
                                }
                        });
        });

                //Create the ad object from the given information
        socket.on('create_book', function (title, isbn, course, price, contact, posted_date, condition) {
                var ad = new AdBook();
                ad.set("Title", title);
                ad.set("ISBN", isbn);
                ad.set("Condition", condition);
                ad.set("Course", course);
                ad.set("Price", price);
                ad.set("Available", "Yes");
                ad.set("Seller", contact);
                ad.set("ID", unique_num);
                ad.set("PostedDate", posted_date)
                ad.set("Type", "Book");
                unique_num++;
                ad.save();
        })

        socket.on('request_lease', function () {

                var queryLease = new Parse.Query(AdLease);  //Creates a query for the Ad class
                queryLease.containedIn("Type", ["Lease"]);
                queryLease.containedIn("Available", ["Yes"]);
                //queryLease.equalTo("Rooms", rooms);
                //queryLease.lessThanOrEqualTo("Price", price);
                queryLease.ascending("Price");


                queryLease.find({
                         success: function(results){  //saves the search to results
                                socket.emit('receive_leases', results);
                                }
                        });
        });

                //Create the ad object from the given information
        socket.on('create_lease', function (address, date, style, rooms, desc, price, contact) {
                var ad = new AdLease();
                ad.set("Address", address);
                ad.set("LeaseDate", date);
                ad.set("LeaseStyle", style);
                ad.set("Rooms", rooms);
                ad.set("Description", desc);
                ad.set("Price", price);
                ad.set("Available", "Yes");
                ad.set("Seller", contact);
                ad.set("ID", unique_num);
                ad.set("Type", "Lease");
                unique_num++;
                ad.save();
        })

        socket.on('create_user', function (uniqname, password){
                var user = new User();
                user.set("Uniqname", email);
                user.set("Password", password);
                user.save();
        })

        socket.on('user_exist', function (uniqname, password){
                var queryUsers = new Parse.Query(user);
                queryUsers.equalTo("Uniqname", uniqname);
                queryUsers.equalTo("password", password);
                queryUsers.find({
                    success: function(results){
                        socket.emit('user_exist_success', results);
                    }
                });
        })

        // socket.on('see_my_sales', function (email, avail){
        //         var queryAllAds = new Parse.Query(Ad);  //Creates a query for the Ad class
        //         queryAllAds.equalTo("Seller", email);
        //         queryAllAds.containedIn("Available", avail);
        //         queryAllAds.find({
        //                  success: function(results){  //saves the search to results
        //                  socket.emit('personal_list_of_sales', results);
        //                 }
        //         });

        // })

         socket.on('see_my_sales', function (email){
                var queryLeases = new Parse.Query(AdLease);
                queryLeases.equalTo("Seller", email);
                queryLeases.containedIn("Available", ["Yes"]);
                queryLeases.find({
                         success: function(results){  //saves the search to results
                         socket.emit('personal_list_of_sales_leases', results);
                        }
                });
                var queryBooks = new Parse.Query(AdBook);
                queryBooks.equalTo("Seller", email);
                queryBooks.containedIn("Available", ["Yes"]);
                 queryBooks.find({
                         success: function(results){  //saves the search to results
                         socket.emit('personal_list_of_sales_books', results);
                        }
                });
                var queryTickets = new Parse.Query(AdTicket);
                queryTickets.containedIn("Available", ["Yes"]);
                queryTickets.equalTo("Seller", email);
                queryTickets.find({
                        success: function(results){  //saves the search to results
                        socket.emit('personal_list_of_sales_tickets', results);
                        }
                });


        })
        //Might possibly need to make this create bid for tickets
        socket.on('create_ticket_bid', function (id, email, offer, title, date, type, price, seat){
                console.log("Create Bid Contacted");
                //var queryAllAds = new Parse.Query(Ad);  //Creates a query for the Ad class
                //queryAllAds.equalTo("ID", id);
                var bid = new Bid;
                bid.set("ID", id);
                //Changed from BuyerContact to Buyer
                bid.set("Buyer", email);
                bid.set("Offer", offer);
                bid.set("Event", title);
                bid.set("EventDate", date);
                bid.set("Price", price);
                bid.set("Seat", seat);
                bid.set("TicketType", type);
                bid.save();
        })

                socket.on('create_book_bid', function (id, email, offer, title, isbn, course, price, condition){
                console.log("Create Bid Contacted");
                //var queryAllAds = new Parse.Query(Ad);  //Creates a query for the Ad class
                //queryAllAds.equalTo("ID", id);
                var bid = new Bid;
                bid.set("ID", id);
                //Changed from BuyerContact to Buyer
                bid.set("Buyer", email);
                bid.set("Offer", offer);
                bid.set("Title", title);
                bid.set("ISBN", isbn);
                bid.set("Price", price);
                bid.set("Condition", condition);
                bid.set("Course", course);
                bid.save();
                // queryAllAds.find({
                //          success: function(results){  //saves the search to results
                //                 results[0].set("Bids", results[0].get("Bids").concat(bid));
                //         }
                // });
        })

        // socket.on('see_my_bids', function (email, avail){
        //         var queryAllAds = new Parse.Query(Ad);  //Creates a query for the Ad class
        //         queryAllAds.equalTo("Buyer", email);
        //         queryAllAds.containedIn("Available", avail);
        //         queryAllAds.find({
        //                  success: function(results){  //saves the search to results
        //                  socket.emit('personal_list_of_bids', results);
        //                 }
        //         });
        // })

        socket.on('see_my_bids', function (email){
                //var queryLeases = new Parse.Query(AdLease);
                // queryLeases.equalTo("Buyer", email);
                // queryLeases.find({
                //          success: function(results){  //saves the search to results
                //          socket.emit('personal_list_of_bids_leases', results);
                //         }
                // });
                // var queryBooks = new Parse.Query(AdBook);
                // queryBooks.equalTo("Buyer", email);
                //  queryBooks.find({
                //          success: function(results){  //saves the search to results
                //          socket.emit('personal_list_of_bids_books', results);
                //         }
                // });
                var queryBids = new Parse.Query(Bid);
                queryBids.equalTo("Buyer", email);
                queryBids.find({
                        success: function(results){  //saves the search to results
                        socket.emit('personal_list_of_bids_tickets', results);
                        }
                });
        })
        
        // socket.on('see_my_specific_bid_ticket', function (email, id){
        //         console.log("Specific bid ticket worked");
        //         var queryBids = new Parse.Query(Bid);
        //         queryBids.equalTo("Buyer", email);
        //         queryBids.equalTo("ID", id);
        //         queryBids.find({
        //                  success: function(results){  //saves the search to results
        //                  socket.emit('personal_list_of_bids', results);
        //                 }
        //         });
        // })
        socket.on('check_bid', function (uniqueID, email) {
            console.log(uniqueID);
            console.log(email);
            var query = new Parse.Query(Bid);
            query.equalTo("Buyer", email);
            query.equalTo("ID", uniqueID);
            query.find({
              success: function(allBids) {
                console.log(allBids.length);
                if (allBids.length == 1) {
                        socket.emit('found_bid', "Yes");
                        console.log("Return Yes");
                        return;
                }
                else {   
                console.log("Return No");
                socket.emit('found_bid', "No");
            }
        }
              
            });
        })

        // socket.on('see_bids', function (id){
        //         var queryAllAds = new Parse.Query(Ad);
        //         queryAllAds.equalTo("ID", id);
        //         queryAllAds.find({
        //                  success: function(results){  //saves the search to results
        //                  socket.emit('list_of_bids', results.get("Bids"));
        //                 }
        //         });
        // })

         socket.on('see_bids', function (id){
                var queryAllBids = new Parse.Query(Bid);
                queryAllBids.equalTo("ID", id);
                queryAllBids.find({
                         success: function(results){  //saves the search to results
                         socket.emit('list_of_bids', results);
                        }
                });
        })

        // socket.on('request_bio_info', function (uniqueID) {
        //     var query = new Parse.Query(AdTicket);
        //     query.containedIn("Type", ["Ticket"]);
        //     query.equalTo("Event", "MSU");
        //     //query.equalTo("ID", uniqueID);
        //     query.find( {
        //       success: function(ticketObj) {
        //         console.log("IT WORKED");
        //         console.log(ticketObj);
        //         // The object was retrieved successfully.
        //         socket.emit('receive_bio_info', ticketObj);
        //       },
        //       error: function(object, error) {
        //         console.log("ERROR");
        //         // The object was not retrieved successfully.
        //         // error is a Parse.Error with an error code and message.
        //       }
        //     });

        // })

        socket.on('request_specific_ticket', function (uniqueID) {
                var queryTicket = new Parse.Query(AdTicket);  //Creates a query for the Ad class
                queryTicket.equalTo("ID", uniqueID);
                //queryTicket.equalTo("ID", uniqueID);
                //console.log(queryTicket.equalTo("ID", uniqueID));
                queryTicket.find({
                         success: function(results){  //saves the search to results
                            console.log("IT WORKED");
                            console.log(results);
                            socket.emit('receive_specific_ticket', results);
                            },
                            error: function(object, error) {
                            console.log("Error");
                            // The object was not retrieved successfully.
                            // error is a Parse.Error with an error code and message.
                            }
                        });
                    })
        
        socket.on('request_specific_book', function (uniqueID) {
                var queryBook = new Parse.Query(AdBook);  //Creates a query for the Ad class
                queryBook.equalTo("ID", uniqueID);
                //queryTicket.equalTo("ID", uniqueID);
                //console.log(queryTicket.equalTo("ID", uniqueID));
                queryBook.find({
                         success: function(results){  //saves the search to results
                            console.log("IT WORKED");
                            console.log(results);
                            socket.emit('receive_specific_book', results);
                            },
                            error: function(object, error) {
                            console.log("Error");
                            // The object was not retrieved successfully.
                            // error is a Parse.Error with an error code and message.
                            }
                        });
                    })

        socket.on('request_specific_lease', function (uniqueID) {
                var queryLease = new Parse.Query(AdLease);  //Creates a query for the Ad class
                queryLease.equalTo("ID", uniqueID);
                //queryTicket.equalTo("ID", uniqueID);
                //console.log(queryTicket.equalTo("ID", uniqueID));
                queryLease.find({
                         success: function(results){  //saves the search to results
                            console.log("IT WORKED");
                            console.log(results);
                            socket.emit('receive_specific_lease', results);
                            },
                            error: function(object, error) {
                            console.log("Error");
                            // The object was not retrieved successfully.
                            // error is a Parse.Error with an error code and message.
                            }
                        });
                    })
        socket.on('search_tickets', function (event_title) {
            var queryTicket = new Parse.Query(AdTicket);
            queryTicket.equalTo("Event", event_title);
            queryTicket.find({
                success: function(results){  //saves the search to results
                    console.log("IT WORKED");
                    console.log(results);
                    socket.emit('receive_search_ticket_results', results);
                },
                error: function(object, error) {
                    console.log("Error");
                    // The object was not retrieved successfully.
                    // error is a Parse.Error with an error code and message.
                }
            });

        })

         socket.on('remove_ticket_ad', function (uniqueID) {
            var query = new Parse.Query(AdTicket);
            query.equalTo("ID", uniqueID);
            query.find({
              success: function(ticketObj) {
                // The object was retrieved successfully.
                console.log("Ticket not available");
                console.log(ticketObj[0].get("Available"));
                ticketObj[0].set("Available", "No");
                ticketObj[0].set("Event", "changed_event");
                console.log(ticketObj[0].get("Available"));
                ticketObj[0].save();
              }
            });
        })

        socket.on('remove_book_ad', function (uniqueID) {
            var query = new Parse.Query(AdBook);
            query.equalTo("ID", uniqueID);
            query.find({
              success: function(Obj) {
                // The object was retrieved successfully.
                Obj[0].set("Available", "No");
              }
            });
        })

        socket.on('remove_lease_ad', function (uniqueID) {
            var query = new Parse.Query(AdLease);
            query.equalTo("ID", uniqueID);
            query.find({
              success: function(Obj) {
                // The object was retrieved successfully.
                Obj[0].set("Available", "No");
              }
            });
        })

        //******************************************************up
        //*******************************************************


});