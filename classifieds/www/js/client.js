/*USED FOR PUTTING AN AD ON THE MARKET (sell_ticket.html, sell_housing.html, sell_book.html)*/

$(function() {

  var serverURL = 'http://10.5.8.53:8080/';

  // Initialize socket connection
  var socket = io(serverURL);

  


  /**JS for sell_ticket.html....puts ticket on the market**/

  //Sell ticket info goes to server
  $('.ticket_sell_submit').click(function() {

    //Take today's date
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = mm+'-'+dd;

    // Get what the seller typed in the input form
    var ticket_title = $('.ticket_sell_title').val();
    var ticket_event_date = $('.ticket_sell_date').val();
    var ticket_posted_date = today;
    var ticket_type = $('.ticket_sell_type').val();
    var ticket_price = $('.ticket_sell_preferred_price').val();
    var ticket_seat = $('.ticket_sell_seat').val();
    var ticket_seller = localStorage.username;

    // Log ticket info into consoleis
    console.log("Submitting message to server:");
    console.log("Title: " + ticket_title);
    console.log("Event Date: " + ticket_event_date);
    console.log("Posted Date: " + ticket_posted_date);
    console.log("Type: " + ticket_type);
    console.log("Price: " + ticket_price);
    console.log("Seat: " + ticket_seat);
    console.log("Contact: " + ticket_seller);

    // Send ticket sell info to the server to store it.
    socket.emit('create_ticket', ticket_title, ticket_event_date, ticket_type, ticket_price, ticket_seat, ticket_seller, ticket_posted_date);

  });





/**JS for sell_book.html....puts book on the market**/


  $('.book_sell_submit').click(function() {

    //Take today's date
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = mm+'-'+dd;

    // Get what the user typed in the input form
    var book_title = $('.book_sell_title').val();
    var book_isbn = $('.book_sell_isbn').val();
    var book_course = $('.book_sell_course').val();
    var book_preferred_price = $('.book_sell_preferred_price').val();
    var book_contact_seller = localStorage.username;
    var book_posted_date = today;
    var book_condition = $('book_sell_condition').val();



    // Log it in console to see what it is
    console.log("Submitting message to server:");
    console.log("Title: " + book_title);
    console.log("ISBN: " + book_isbn);
    console.log("Course: " + book_course);
    console.log("Price: " + book_preferred_price);
    console.log("Contact: " + book_contact_seller);
    console.log("Posted Date: " + book_posted_date);
    console.log("Condition" + book_condition);

    socket.emit('create_book', book_title, book_isbn, book_course, book_preferred_price, book_contact_seller, book_posted_date, book_condition);
    
  });







/**JS for sell_housing.html....puts sublet on the market**/
 

 $('.housing_sell_submit').click(function() {

    // Get current time
    //var dt = new Date();
    //var timestamp = dt.toUTCString();

    // Get what the user typed in the input form
    var housing_address = $('.housing_sell_address').val();
    var housing_date = $('.housing_sell_date').val();
    var housing_style= $('.housing_sell_style').val();
    var housing_rooms = $('.housing_sell_rooms').val();
    var housing_desc = $('.housing_sell_desc').val();
    var housing_preferred_price = $('.housing_sell_preferred_price').val();
    var housing_contact_seller = localStorage.username;



    // Log it in console to see what it is
    console.log("Submitting message to server:");
    console.log("Address: " + housing_address);
    console.log("Date: " + housing_date);
    console.log("Style: " + housing_style);
    console.log("Rooms: " + housing_rooms);
    console.log("Desc: " + housing_desc);
    console.log("Price: " + housing_preferred_price);
    console.log("Contact: " + housing_contact_seller);

    // Send it to the server to store it.
    socket.emit('create_lease', housing_address, housing_date, housing_style, housing_rooms, housing_desc, housing_preferred_price, housing_contact_seller);

  });

});

