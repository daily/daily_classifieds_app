  var serverURL = 'http://10.5.8.53:8080/';

  // Initialize socket connection
  var socket = io(serverURL);


  //OAUTH LOGIN STUFF
	
	//this is called after the google button is pressed
	function login(){
		var GoogleAuth = gapi.auth2.getAuthInstance();

		GoogleAuth.currentUser.listen(function(user){
			var GoogleUser = user;
			var basicProfile = GoogleUser.getBasicProfile();
			localStorage.username = basicProfile.getEmail();
			console.log(basicProfile);
		})
		GoogleAuth.signIn().then(function(res){
		})

	}
	function logout(){
		var GoogleAuth = gapi.auth2.getAuthInstance();

		GoogleAuth.signOut().then(function(){
			console.log('signed out');
		})
	}

	//called after google js file is loaded
	function init(){
		gapi.load('auth2', function(){
			gapi.auth2.init({
				client_id: '155637095151-047tl8094e5s25l9ou5ndt93vu95q45p.apps.googleusercontent.com',
				hosted_domain: 'umich.edu'
			});
		});
	}

  //

  $('.login_submit').click(function() {
  	if (typeof(Storage) != "undefined") {
  		var username = $('.login_username').val();
  		// Store
  		localStorage.username = ($('.login_username').val()) + '@umich.edu';
  		console.log(localStorage.username)
  			//location.href = "index.html";
  		console.log("IT WORKED");
  		// Retrieve
  		//document.getElementById("result").innerHTML = localStorage.getItem("lastname");
  	} else {
  		console.log("Didn't Work");
  		//document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
  	}
  });

  $('.sign_up').click(function() {
  	var list = $(".login_content");
  	list.empty();
  	console.log("Clear Login Content");
  	var appendLoginText = '<h1><a href="index.html">BlueBuy</a></h1>';
  	appendLoginText += '<form class="login_info" action="">';
  	appendLoginText += '<br>';
  	appendLoginText += '<input type="text" class="sign_up_username" name="sign_up_username"  placeholder="Input Uniqname"  value="" maxlength="25" required>';
  	appendLoginText += '<br>';
  	appendLoginText += '<input type="text" class="sign_up_password" name="sign_up_password" placeholder="Input Password" value="" required>';
  	appendLoginText += '<br>';
  	appendLoginText += '<input type="submit" class="sign_up_submit" name="sign_up_submit" value="Sign Up">';
  	appendLoginText += '</form>';
  	appendLoginText += '<h5 class="forgot_password">Forgot Password?</h5>';
  	appendLoginText += '<h5 class="log_in">Log In</h5>';
  	$(".login_content").append(appendLoginText);
  });

  $('.log_in').click(function() {
  	console.log("Pressed log_in");
  	var list = $(".login_content");
  	list.empty();
  	console.log("Clear Login Content");
  	var appendLoginText = '<h1><a href="index.html">BlueBuy</a></h1>';
  	appendLoginText += '<form class="login_info" action="">';
  	appendLoginText += '<br>';
  	appendLoginText += '<input type="text" class="login_username" name="login_username"  placeholder="Uniqname"  value="" maxlength="25" required>';
  	appendLoginText += '<br>';
  	appendLoginText += '<input type="text" class="login_password" name="login_password" placeholder="Password" value="" required>';
  	appendLoginText += '<br>';
  	appendLoginText += '<input type="submit" class="login_submit" name="login_submit" value="Log In">';
  	appendLoginText += '</form>';
  	appendLoginText += '<h5 class="forgot_password">Forgot Password?</h5>';
  	appendLoginText += '<h5 class="sign_up">Sign Up</h5>';
  	$(".login_content").append(appendLoginText);
  });

  $('.forgot_password').click(function() {
  	console.log("Clicked forgot_password");
  });

  $(function() {

  	$('.sign_up_submit').click(function() {

  		console.log("sign_up worked");
  		var username = $('.sign_up_username').val();
  		var password = $('.sign_up_password').val();

  		console.log("Submitting user info to server:");
  		console.log("Username: " + username);
  		console.log("Password: " + password);

  		// Send ticket sell info to the server to store it.
  		socket.emit('create_user', username, password);

  	})

  });
