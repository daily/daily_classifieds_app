/*USED TO SEE ALL BIDS MADE BY THE USER (bid_statuses.html)*/

  var serverURL = 'http://10.5.8.53:8080/';

  // Initialize socket connection
  var socket = io(serverURL);

  
  // Clear all user's bids in order to make fresh page
  var list = $(".bid_content");
   list.empty();
   console.log("Clear User's Bids");

   // Request all sales from the server
   socket.emit('see_my_bids', localStorage.username);

  // Perform this code block whenever the server sends all tickets
  socket.on('personal_list_of_bids_tickets', function(results) {
    console.log("Grabbing all tickets from server...");
    console.log("List of tickets: ");

    // Grab the data from each of the tickets
    for (var i = 0; i < results.length; ++i) {
      
      // Saves ticket data from server into vars
      var ticket_title = results[i].Event;
      var ticket_date = results[i].EventDate;
      var ticket_type = results[i].TicketType;
      var ticket_price = results[i].Price;
      var ticket_seat = results[i].Seat;
      var ticket_id = results[i].ID;
      var ticket_offer = results[i].Offer

      console.log("Title: " + ticket_title);
      console.log("Date: " + ticket_date);
      console.log("Type: " + ticket_type);
      console.log("Price: " + ticket_price);
      console.log("Seat: " + ticket_seat);
      console.log("ID: " + ticket_id);
      console.log("Offer: " + ticket_offer);

      // Format the data grabbed from the tickets array
      var appendText = '<div class="ticket_div">';
      appendText += '<div class="date">';
      appendText += '<p>Event Date</p>';
      appendText += '<h5>' + ticket_date.substring(5, 10) + '</h5>';
      appendText += '</div>';
      appendText += '<div class="ticket_description">'
      appendText += '<p class="ticket_category">' + ticket_type + '</p>';
      appendText += '<p class="ticket_specifics"><a #="' + ticket_id + '" href="ticket_bio.html' + '?id=' + ticket_id + '">' + ticket_title + '</a></p>';
      appendText += '<div class="ticket_status">You bid $' + ticket_offer + '</div>';
      appendText += '<p class="ticket_price">$' + ticket_price + '</p>';
      appendText += '</div>';
      appendText += '</div>';
      console.log(appendText);

      // For each ticket logged, append it to the .all_tickets div
      $(".bid_content").append(appendText);
    }
  });

socket.on('personal_list_of_bids_books', function(book_results) {
    console.log("Grabbing all books from server...");
    console.log("List of books: ");

    for (var i = 0; i < book_results.length; ++i) {
      // Grab the data from messages

      var book_title = book_results[i].Title;
      var book_isbn = book_results[i].ISBN;
      var book_course = book_results[i].Course;
      var book_preferred_price = book_results[i].Price;
      var book_id = book_results[i].ID;
      var book_condition = book_results[i].Condition;


      console.log("Title: " + book_title);
      console.log("ISBN: " + book_isbn);
      console.log("Course: " + book_course);
      console.log("Price: " + book_preferred_price);
      console.log("ID: " + book_id);
      console.log("Condition: " + book_condition);

      // Format the data grabbed from the messages array
      var appendTextBook = '<div class="book_div">';
      appendTextBook += '<div class="book_description">';
      appendTextBook += '<p class="book_course">' + book_course + '</p>';
      appendTextBook += '<p class="book_specifics"><a #="' + book_id + '" href="book_bio.html' + '?id=' + book_id + '">' + book_title + '</a></p>';
      appendTextBook += '<p class="book_price">$' + book_preferred_price + '</p>';
      appendTextBook += '</div>';
      appendTextBook += '</div>';
      console.log(appendTextBook);

      // For each message logged, append it to the .list-of-messages div
      $(".bid_content").append(appendTextBook);
    }
  });


socket.on('personal_list_of_bids_leases', function(lease_results) {
    console.log("Grabbing all leases from server...");
    console.log("List of leases: ");

    for (var i = 0; i < lease_results.length; ++i) {
      // Grab the data from messages

      var housing_address = lease_results[i].Address;
      var housing_date = lease_results[i].LeaseDate;
      var housing_style = lease_results[i].LeaseStyle;
      var housing_rooms = lease_results[i].Rooms;
      var housing_desc = lease_results[i].Description;
      var housing_preferred_price = lease_results[i].Price;
      var housing_contact_seller = lease_results[i].Seller;
      var housing_id = lease_results[i].ID;

      console.log("Address: " + housing_address);
      console.log("Date: " + housing_date);
      console.log("Style: " + housing_style);
      console.log("Rooms: " + housing_rooms);
      console.log("Desc: " + housing_desc);
      console.log("Price: " + housing_preferred_price);
      console.log("Contact: " + housing_contact_seller);
      console.log("ID: " + housing_id);

      // Format the data grabbed from the messages array
      var appendTextLease = '<div class="lease_div">';
      appendTextLease += '<div class="lease_description">';
      appendTextLease += '<p class="lease_style">' + housing_style + '</p>';
      appendTextLease += '<p class="lease_address"><a #="' + housing_id + '" href="housing_bio.html' + '?id=' + housing_id + '">' + housing_address + '</a></p>';
      appendTextLease += '<p class="lease_price">$' + housing_preferred_price + '</p>';
      appendTextLease += '</div>';
      appendTextLease += '</div>';
      console.log(appendTextLease);

      // For each message logged, append it to the .list-of-messages div
      $(".bid_content").append(appendTextLease);

      }
    });  