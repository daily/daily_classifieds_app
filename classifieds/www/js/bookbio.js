/*USED TO SET UP BIO OF ANY BOOK (book_bio.html)*/

  var serverURL = 'http://10.5.8.53:8080/';

  // Initialize socket connection
  var socket = io(serverURL);

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(function() {
	// Step 1: Get id from URL (query parameter)
	var uniqueID = getParameterByName('id',window.location.href);
	uniqueID = parseInt(uniqueID);
	console.log("UNIQUEID: " + uniqueID);

	// Step 2: Send id to server, ask for information from that id
	socket.emit('request_specific_book', uniqueID);
	console.log("SENT TO SERVER");

	// Step 3: Put information (title, description, etc.) where they need to go
	socket.on('receive_specific_book', function(uniqueBookObj) {
		console.log("BACK FROM SERVER");
		var book_title = uniqueBookObj[0].Title;
		var book_isbn = uniqueBookObj[0].ISBN;
		var book_course = uniqueBookObj[0].Course;
	    var book_price = uniqueBookObj[0].Price;
	    var book_posted_date = uniqueBookObj[0].PostedDate;
	    var book_seller = uniqueBookObj[0].Seller;
	    var book_condition = uniqueBookObj[0].Condition;


		console.log("Title: " + book_title);
		console.log("ISBN: " + book_isbn);
	    console.log("Course: " + book_course);
	    console.log("Price: " + book_price);
	    console.log("Posted Date: " + book_posted_date);
	    console.log("Condition: " + book_condition);
	
	    
	    var appendTextBidBook = '<div class="ticket_bio_type">' + book_course + '</div>';
	    appendTextBidBook += '<div class="ticket_bio_header">';
	    appendTextBidBook += '<p class="ticket_bio_name">' + book_title + '</p>';
	    appendTextBidBook += '<p class="ticket_bio_price">$' + book_price + '</p>';
	    	    if (localStorage.username == book_seller) {
	    	console.log("This is Your Book");
	    	appendTextBidBook += '<div class="ticket_bio_no_bid">Your Book</div>';
	    	appendTextBidBook += '<p class="ticket_bio_date">Posted ' + book_posted_date + '</p>';
	    appendTextBidBook += '</div>';
	    appendTextBidBook += '<div class="specific_ticket_bio_description">';
	    appendTextBidBook += '<h3>Description</h3>';
	    appendTextBidBook += '<p><i>Course - ' + book_course + '</i></p>';
	    appendTextBidBook += '<p><b>ISBN: </b>' + book_isbn + '</p>';
	    appendTextBidBook += '<p><b>Condition: </b>' + book_condition + '</p>';
	    appendTextBidBook += '</div>';
	    console.log("Print book information");
	    $(".book_content").append(appendTextBidBook);
		}
		else {
			socket.emit('check_bid', ticket_id, localStorage.username);
			socket.on('found_bid', function(book_bid_found) {
			foundBook = book_bid_found;
			console.log(foundBook);
			if (foundBook == "Yes") {
				console.log("Your have already bid on this book");
				appendTextBidBook += '<div class="ticket_bio_no_bid">You Already Bid</div>';
				
			}
			
			else {
				console.log("You have not bid on this ticket");
				appendTextBidBook += '<button class="bid_book" data-toggle="modal" data-target="#myModal">BID</button>';
			}
			appendTextBidBook += '<p class="ticket_bio_date">Posted ' + book_posted_date + '</p>';
	    	appendTextBidBook += '</div>';
	    	appendTextBidBook += '<div class="specific_ticket_bio_description">';
	    	appendTextBidBook += '<h3>Description</h3>';
	    	appendTextBidBook += '<p><i>Course - ' + book_course + '</i></p>';
	    	appendTextBidBook += '<p><b>ISBN: </b>' + book_isbn + '</p>';
	    	appendTextBidBook += '<p><b>Condition: </b>' + book_condition + '</p>';
	    	appendTextBidBook += '</div>';
	    	console.log("Print book information");
	    	$(".book_content").append(appendTextBidBook);
			})
		}

	    $('.bid_submit').click(function() {
	    console.log("Pressed Submit");
	    var uniqueID = getParameterByName('id',window.location.href);
	    uniqueID = parseInt(uniqueID);
	    var book_bid = $('.bid_number').val();
	    console.log(uniqueID + " " + book_bid + " " + localStorage.username);
	    $('#myModal').modal('hide');
	    
	    socket.emit('create_book_bid', uniqueID, localStorage.username, book_bid, book_title, book_isbn, book_course, book_price, book_condition);
		console.log("Sent Bid to Server");

		});  
	})
});
	    appendTextBidBook += '<button class="bid_ticket" data-toggle="modal" data-target="#myModal">BID</button>';
	    appendTextBidBook += '<p class="product_posted_date">Posted ' + book_posted_date + '</p>';
	    appendTextBidBook += '</div>';
	    appendTextBidBook += '<div class="specific_product_description">';
	    appendTextBidBook += '<h3>Description</h3>';
	    appendTextBidBook += '<p><i>Book - ' + book_course + '</i></p>';
	    appendTextBidBook += '<p><b>ISBN: </b>' + book_isbn + '</p>';
	    appendTextBidBook += '</div>';
	    console.log("Print book information");
	    $(".book_content").append(appendTextBidBook);
	})
});