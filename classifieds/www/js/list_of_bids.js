 /**USED TO OBTAIN ALL BIDS FOR A CERTAIN AD FROM DATABASE (list_of_bids.html)**/

  var serverURL = 'http://10.5.8.53:8080/';

  // Initialize socket connection
  var socket = io(serverURL);



function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(function() {
	// Step 1: Get id from URL (query parameter)

	var uniqueID = getParameterByName('id',window.location.href);
	uniqueID = parseInt(uniqueID);
	console.log("UNIQUEID: " + uniqueID);

	// Step 2: Send id to server, ask for information from that id
	socket.emit('see_bids', uniqueID);
	console.log("SENT TO SERVER");
	// Step 3: Put information (title, description, etc.) where they need to go
	socket.on('list_of_bids', function(results) {
    console.log("Grabbing all bids from server...");
    console.log("List of bids: ");

    for (var i = 0; i < results.length; ++i) {
      // Grab the data from messages

      var bid_buyer = results[i].Buyer;
      var bid_offer = results[i].Offer;


      console.log("Buyer: " + bid_buyer);
      console.log("Offer: " + bid_offer);

      // Format the data grabbed from the messages array
      var appendTextBid = '<div class="specific_bid">';
      appendTextBid += '<p>Bidder: ' + bid_buyer + '</p>';
      appendTextBid += '<p>Offer: $' + bid_offer + '</p>';
      appendTextBid += '</div>';
      console.log(appendTextBid);

      // For each message logged, append it to the .list-of-bids-content div
      $(".list_of_bids_content").append(appendTextBid);
    }
  });
});