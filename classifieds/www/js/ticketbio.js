/*USED TO SET UP BIO OF ANY TICKET (ticket_bio.html)*/

  var serverURL = 'http://10.5.8.53:8080/';

  // Initialize socket connection
  var socket = io(serverURL);



function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$(function() {
	
	// Step 1: Get id from URL (query parameter)
	var uniqueID = getParameterByName('id',window.location.href);
	uniqueID = parseInt(uniqueID);
	console.log("UNIQUEID: " + uniqueID);
	var foundTicket = "asdf";
	// Step 2: Send id to server, ask for information from that id
	socket.emit('request_specific_ticket', uniqueID);
	console.log("SENT TO SERVER");
	// Step 3: Put information (title, description, etc.) where they need to go
	socket.on('receive_specific_ticket', function(uniqueTicketObj) {
		console.log("BACK FROM SERVER");
		var ticket_title = uniqueTicketObj[0].Event;
		var ticket_event_date = uniqueTicketObj[0].EventDate;
		var ticket_posted_date = uniqueTicketObj[0].PostedDate;
		var ticket_seller = uniqueTicketObj[0].Seller;
		var ticket_type = uniqueTicketObj[0].TicketType;
	    var ticket_price = uniqueTicketObj[0].Price;
	    var ticket_seat = uniqueTicketObj[0].Seat;
	    var ticket_id = uniqueTicketObj[0].ID;


		console.log("Title: " + ticket_title);
		console.log("Event Date: " + ticket_event_date);
		console.log("Posted Date: " + ticket_posted_date);
		console.log("Seller: " + ticket_seller);
	    console.log("Type: " + ticket_type);
	    console.log("Price: " + ticket_price);
	    console.log("Seat: " + ticket_seat);
	    console.log("ID: " + ticket_id);
		
	    var appendTextBidTicket = '<div class="ticket_bio_type">' + ticket_type + '</div>';
	    appendTextBidTicket += '<div class="ticket_bio_header">';
	    appendTextBidTicket += '<p class="ticket_bio_name">' + ticket_title + '</p>';
	    appendTextBidTicket += '<p class="ticket_bio_price">$' + ticket_price + '</p>';
	    if (localStorage.username == ticket_seller) {
	    	console.log("This is Your Ticket");
	    	appendTextBidTicket += '<div class="ticket_bio_no_bid">Your Ticket</div>';
	    	appendTextBidTicket += '<p class="ticket_bio_date">Posted ' + ticket_posted_date + '</p>';
	    appendTextBidTicket += '</div>';
	    appendTextBidTicket += '<div class="specific_ticket_bio_description">';
	    appendTextBidTicket += '<h3>Description</h3>';
	    appendTextBidTicket += '<p><i>Ticket - ' + ticket_type + '</i></p>';
	    appendTextBidTicket += '<p><b>SEATING ASSIGNMENT: </b>' + ticket_seat + '</p>';
	    appendTextBidTicket += '<p><b>EVENT DATE: </b>' + ticket_event_date + '</p>';
	    appendTextBidTicket += '</div>';
	    console.log("Print ticket information");
	    $(".ticket_bio_content").append(appendTextBidTicket);
		}
		else {
			socket.emit('check_bid', ticket_id, localStorage.username);
			socket.on('found_bid', function(ticket_bid_found) {
			foundTicket = ticket_bid_found;
			console.log(foundTicket);
			if (foundTicket == "Yes") {
				console.log("Your have already bid on this ticket");
				appendTextBidTicket += '<div class="ticket_bio_no_bid">You Already Bid</div>';
				
			}
			
			else {
				console.log("You have not bid on this ticket");
				appendTextBidTicket += '<button class="bid_ticket" data-toggle="modal" data-target="#myModal">BID</button>';
			}
			appendTextBidTicket += '<p class="ticket_bio_date">Posted ' + ticket_posted_date + '</p>';
	    	appendTextBidTicket += '</div>';
	    	appendTextBidTicket += '<div class="specific_ticket_bio_description">';
	    	appendTextBidTicket += '<h3>Description</h3>';
	    	appendTextBidTicket += '<p><i>Ticket - ' + ticket_type + '</i></p>';
	    	appendTextBidTicket += '<p><b>SEATING ASSIGNMENT: </b>' + ticket_seat + '</p>';
	    	appendTextBidTicket += '<p><b>EVENT DATE: </b>' + ticket_event_date + '</p>';
	    	appendTextBidTicket += '</div>';
	    	console.log("Print ticket information");
	    	$(".ticket_bio_content").append(appendTextBidTicket);
			})
		}

	    $('.bid_submit').click(function() {
	    console.log("Pressed Submit");
	    var uniqueID = getParameterByName('id',window.location.href);
	    uniqueID = parseInt(uniqueID);
	    var ticket_bid = $('.bid_number').val();
	    console.log(uniqueID + " " + ticket_bid + " " + localStorage.username);
	    $('#myModal').modal('hide');
	    
	    socket.emit('create_ticket_bid', uniqueID, localStorage.username, ticket_bid, ticket_title, ticket_event_date, ticket_type, ticket_price, ticket_seat);
		console.log("Sent Bid to Server");

		});  
	})
});