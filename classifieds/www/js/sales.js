/*USED TO SEE ALL OPEN ADS MADE BY THE USER (open_sales.html)*/

  var serverURL = 'http:/10.5.8.53:8080/';

  // Initialize socket connection
  var socket = io(serverURL);

  
  // Clear all sales in order to make fresh page
  var list = $(".open_sales_div");
   list.empty();
   console.log("Clear Sales");

   // Request all sales from the server
   socket.emit('see_my_sales', localStorage.username);

  // Perform this code block whenever the server sends all tickets
  socket.on('personal_list_of_sales_tickets', function open_sales_tickets(results) {
    console.log("Grabbing all tickets from server...");
    console.log("List of tickets: ");

    // Grab the data from each of the tickets
    for (var i = 0; i < results.length; ++i) {
      
      // Saves ticket data from server into vars
      var ticket_title = results[i].Event;
      var ticket_date = results[i].EventDate;
      var ticket_type = results[i].TicketType;
      var ticket_price = results[i].Price;
      var ticket_seat = results[i].Seat;
      var ticket_id = results[i].ID;

      console.log("Title: " + ticket_title);
      console.log("Date: " + ticket_date);
      console.log("Type: " + ticket_type);
      console.log("Price: " + ticket_price);
      console.log("Seat: " + ticket_seat);
      console.log("ID: " + ticket_id);

      // Format the data grabbed from the tickets array
      var appendText = '<div class="ticket_div">';
      appendText += '<div class="ticket_description">';
      appendText += '<div class="date">';
      appendText += '<p>Event Date</p>';
      appendText += '<h5>' + ticket_date.substring(5, 10) + '</h5>';
      appendText += '</div>';
      appendText += '<p class="ticket_category">' + ticket_type + '</p>';
      appendText += '<p class="ticket_specifics"><a #="' + ticket_id + '" href="list_of_bids.html' + '?id=' + ticket_id + '">' + ticket_title + '</a></p>';
      appendText += '<p class="ticket_price">$' + ticket_price + '</p>';
      appendText += '</div>';
      appendText += '<div><i class="material-icons edit_delete" style="font-size:45px;color:black;display:none">delete</i></div>';
      appendText += '</div>';
      console.log(appendText);

      // For each ticket logged, append it to the .all_tickets div
      $(".open_sales_div").append(appendText);

       $('.edit_delete').click(function (){
          socket.emit('remove_ticket_ad', ticket_id);
          console.log("Ticket Removed");
          location.reload();
        }); 

    }
  });

socket.on('personal_list_of_sales_books', function(book_results) {
    console.log("Grabbing all books from server...");
    console.log("List of books: ");

    for (var i = 0; i < book_results.length; ++i) {
      
      // Grab the data from each of the books
      var book_title = book_results[i].Title;
      var book_isbn = book_results[i].ISBN;
      var book_course = book_results[i].Description;
      var book_preferred_price = book_results[i].Price;
      var book_id = book_results[i].ID;
      var book_posted_date = book_results[i].PostedDate;


      console.log("Title: " + book_title);
      console.log("ISBN: " + book_isbn);
      console.log("Course: " + book_course);
      console.log("Price: " + book_preferred_price);
      console.log("ID: " + book_id);
      console.log("Posted Date: " + book_posted_date);

       // Format the data grabbed from the books array
      var appendTextBook = '<div class="book_div">';
      appendTextBook += '<div class="book_date">';
      appendTextBook += '<p>Posted Date</p>';
      appendTextBook += '<h5>' + book_posted_date + '</h5>';
      appendTextBook += '</div>';
      appendTextBook += '<div class="book_description">';
      appendTextBook += '<p class="book_course">' + book_course + '</p>';
      appendTextBook += '<p class="book_specifics"><a #="' + book_id + '" href="list_of_bids.html' + '?id=' + book_id + '">' + book_title + '</a></p>';
      appendTextBook += '<p class="book_price">$' + book_preferred_price + '</p>';
      appendTextBook += '</div>';
      appendTextBook += '<h2><i id="edit_delete" class="material-icons" style="font-size:35px;color:black;display:none">delete</i></h2>'
      appendTextBook += '</div>';
      console.log(appendTextBook);

      // For each book logged, append it to the .open_sales_div
      $(".open_sales_div").append(appendTextBook);
    }
  });


socket.on('personal_list_of_sales_leases', function(lease_results) {
    console.log("Grabbing all leases from server...");
    console.log("List of leases: ");

    for (var i = 0; i < lease_results.length; ++i) {
      
      // Grab the data from each of the leases
      var housing_address = lease_results[i].Address;
      var housing_date = lease_results[i].LeaseDate;
      var housing_style = lease_results[i].LeaseStyle;
      var housing_rooms = lease_results[i].Rooms;
      var housing_desc = lease_results[i].Description;
      var housing_preferred_price = lease_results[i].Price;
      var housing_contact_seller = lease_results[i].Seller;
      var housing_id = lease_results[i].ID;

      console.log("Address: " + housing_address);
      console.log("Date: " + housing_date);
      console.log("Style: " + housing_style);
      console.log("Rooms: " + housing_rooms);
      console.log("Desc: " + housing_desc);
      console.log("Price: " + housing_preferred_price);
      console.log("Contact: " + housing_contact_seller);
      console.log("ID: " + housing_id);

      // Format the data grabbed from the messages array
      var appendTextLease = '<div class="lease_div">';
      appendTextLease += '<div class="lease_description">';
      appendTextLease += '<p class="lease_style">' + housing_style + '</p>';
      appendTextLease += '<p class="lease_address"><a #="' + housing_id + '" href="list_of_bids.html' + '?id=' + housing_id + '">' + housing_address + '</a></p>';
      appendTextLease += '<p class="lease_price">$' + housing_preferred_price + '</p>';
      appendTextLease += '</div>';
      appendTextLease += '</div>';
      console.log(appendTextLease);

      // For each message logged, append it to the .list-of-messages div
      $(".open_sales_div").append(appendTextLease);

      }
    }); 



