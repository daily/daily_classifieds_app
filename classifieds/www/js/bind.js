 /*USED TO OBTAIN ALL AVAILABLE TICKETS, BOOKS, SUBLETS (tickets.html, books.html, housing.html*/

  var serverURL = 'http://10.5.8.53:8080/';

// Initialize socket connection
var socket = io(serverURL);

  // Request all tickets from the server
  socket.emit('request_ticket');

  var ticketFound = "adf";


  // Perform this code block whenever the server sends all tickets (tickets.html)
  socket.on('receive_tickets', function(results) {
    // Clear all tickets in order to make fresh page
    var list = $(".all_tickets");
    list.empty();
    console.log("Clear Tickets");

    console.log("Grabbing all tickets from server...");
    console.log("List of tickets: ");

    // Grab the data from each of the tickets
    for (var i = 0; i < results.length; ++i) {
      
        // Saves ticket data from server into vars
        var ticket_title = results[i].Event;
        var ticket_date = results[i].EventDate;
        var ticket_type = results[i].TicketType;
        var ticket_price = results[i].Price;
        var ticket_seat = results[i].Seat;
        var ticket_id = results[i].ID;
        var ticket_seller = results[i].Seller;

        console.log("Title: " + ticket_title);
        console.log("Date: " + ticket_date);
        console.log("Type: " + ticket_type);
        console.log("Price: " + ticket_price);
        console.log("Seat: " + ticket_seat);
        console.log("ID: " + ticket_id);
        console.log("Seller: " + ticket_seller);

        // Format the data grabbed from the tickets array
        var appendText = '<div class="ticket_div">';
        appendText += '<div class="ticket_description">';
        appendText += '<div class="date">';
        appendText += '<p>Event Date</p>';
        appendText += '<h5>' + ticket_date.substring(5, 10) + '</h5>';
        appendText += '</div>';
        appendText += '<p class="ticket_category">' + ticket_type + '</p>';
        appendText += '<p class="ticket_specifics"><a #="' + ticket_id + '" href="ticket_bio.html' + '?id=' + ticket_id + '">' + ticket_title + '</a></p>';
        if (localStorage.username == ticket_seller) {
            appendText += '<div class="ticket_status">Your Ticket</div>';
            appendText += '<p class="ticket_price">$' + ticket_price + '</p>';
            appendText += '</div>';
            appendText += '</div>';
            console.log(appendText);
            $(".all_tickets").append(appendText);
        }

        else {
            console.log("NOT SELLER");
            console.log(localStorage.username + " " + ticket_id);
            socket.emit('check_ticket_bid', ticket_id, localStorage.username);
            socket.on('found_ticket_bid', function(ticket_bid_found) {
            foundTicket = ticket_bid_found;
            console.log("Found Ticket");
            console.log(foundTicket);
            if (foundTicket == "Yes") {
                appendText += '<div class="ticket_status">You Already Bid</div>';
                
            }
            appendText += '<p class="ticket_price">$' + ticket_price + '</p>';
            appendText += '</div>';
            appendText += '</div>';
            console.log(appendText);
            $(".all_tickets").append(appendText);

        })
      }
    }
  });


/************************************************************************************************************************************************/


// Request all books from the server
socket.emit('request_book');

// Perform this code block whenever the server sends all books
socket.on('receive_books', function(book_results) {
    console.log("Grabbing all books from server...");
    console.log("List of books: ");

    // Clear all books in order to make fresh page
    var list = $(".all_books");
    list.empty();
    console.log("Clear Books");

    // Grab the data for each of the books
    for (var i = 0; i < book_results.length; ++i) {
      
      // Saves book data from server into vars
      var book_title = book_results[i].Title;
      var book_isbn = book_results[i].ISBN;
      var book_condition = book_results[i].Condition;
      var book_course = book_results[i].Course;
      var book_preferred_price = book_results[i].Price;
      var book_seller = book_results[i].Seller;
      var book_id = book_results[i].ID;
      var book_date = book_results[i].PostedDate;


      console.log("Title: " + book_title);
      console.log("ISBN: " + book_isbn);
      console.log("Course: " + book_course);
      console.log("Price: " + book_preferred_price);
      console.log("Contact: " + book_seller);
      console.log("ID: " + book_id);
      console.log("Posted Date: " + book_date);

      // Format the data grabbed from the books array
      var appendText = '<div class="book_div">';
      appendText += '<div class="book_date">';
      appendText += '<p>Posted Date</p>';
      appendText += '<h5>' + book_date + '</h5>';
      appendText += '</div>';
      appendText += '<div class="book_description">'
      appendText += '<p class="book_course">' + book_course + '</p>';
      appendText += '<p class="book_specifics"><a #="' + book_id + '" href="../book_bio.html' + '?id=' + book_id + '">' + book_title + '</a></p>';
      if (localStorage.username == book_seller) {
        appendText += '<div class="book_status">Your Book</div>';
      }
      // else {
      //   console.log("NOT SELLER");
      //   console.log(localStorage.username + " " + ticket_id);
      //   socket.emit('check_ticket_bid', localStorage.username, ticket_id);
      //   console.log('Specific Bid Ticket Worked');
      //   socket.on('found_ticket_bid', function (found_ticket) {
      //     console.log(found_ticket);
      //     var ticket_was_found = found_ticket;
      //     if (ticket_was_found == "Yes") {
      //       appendText += '<div class="ticket_status">Bidding</div>';
      //       console.log("IN THE IF CLAUSE");
      //     }
      //   });
      // }
     
      appendText += '<p class="book_price">$' + book_preferred_price + '</p>';
      appendText += '</div>';
      appendText += '</div>';
      console.log(appendText);

      // For each book logged, append it to the .all_books div
      $(".all_books").append(appendText);
    }
  });


//***********************************************************************************************************************************************/


// Request all leases from the server
socket.emit('request_lease');

// Perform this code block whenever the server sends all leases
socket.on('receive_leases', function(lease_results) {
    console.log("Grabbing all leases from server...");
    console.log("List of leases: ");
    
    // Clear all leases in order to make fresh page
    var list = $(".all_leases");
    list.empty();
    console.log("Clear Leases");

    // Grab the data for each of the books
    for (var i = 0; i < lease_results.length; ++i) {

      // Saves lease data from server into vars
      var housing_address = lease_results[i].Address;
      var housing_date = lease_results[i].LeaseDate;
      var housing_style = lease_results[i].LeaseStyle;
      var housing_rooms = lease_results[i].Rooms;
      var housing_desc = lease_results[i].Description;
      var housing_preferred_price = lease_results[i].Price;
      var housing_contact_seller = lease_results[i].Seller;
      var housing_id = lease_results[i].ID;

      console.log("Address: " + housing_address);
      console.log("Date: " + housing_date);
      console.log("Style: " + housing_style);
      console.log("Rooms: " + housing_rooms);
      console.log("Desc: " + housing_desc);
      console.log("Price: " + housing_preferred_price);
      console.log("Contact: " + housing_contact_seller);
      console.log("ID: " + housing_id);

      // Format the data grabbed from the leases array
      var appendTextLease = '<div class="lease_div">';
      appendTextLease += '<div class="lease_description">';
      appendTextLease += '<p class="lease_style">' + housing_style + '</p>';
      appendTextLease += '<p class="lease_address"><a #="' + housing_id + '" href="housing_bio.html' + '?id=' + housing_id + '">' + housing_address + '</a></p>';
      appendTextLease += '<p class="lease_price">$' + housing_preferred_price + '</p>';
      appendTextLease += '</div>';
      appendTextLease += '</div>';
      console.log(appendTextLease);

      // For each lease logged, append it to the .all_leases div
      $(".all_leases").append(appendTextLease);

      }
    });

    

//*************************************************************************************************************************************************

  // $('#filter-tickets').click(function() {
  //   console.log("Pressed filter tickets");
  //   var text = $('#filter-tickets').text();
  //   $('#filter-tickets').text(
  //       text == "radio_button_checked" ? "radio_button_unchecked" : "radio_button_checked");
  // });

  //  $('#filter-books').click(function() {
  //   console.log("Pressed filter books");
  //   var text = $('#filter-books').text();
  //   $('#filter-books').text(
  //       text == "radio_button_checked" ? "radio_button_unchecked" : "radio_button_checked");
  // });

  //   $('#filter-leases').click(function() {
  //   console.log("Pressed filter leases");
  //   var text = $('#filter-leases').text();
  //   $('#filter-leases').text(
  //       text == "radio_button_checked" ? "radio_button_unchecked" : "radio_button_checked");
  // });

//*************************************************************************************************************************************************


//Searches tickets for specific tickets based on event title
$('.search_ticket_submit').click(function() {
    var list = $(".all_tickets");
    list.empty();
    console.log("Clear Tickets");

    var search_ticket_title = $('.search_ticket_text').val();
    socket.emit('search_tickets', search_ticket_title);
    
    socket.on('receive_search_ticket_results', function(results) {
    console.log("Grabbing all tickets from server...");
    console.log("List of tickets: ");

    // Grab the data from each of the tickets
    for (var i = 0; i < results.length; ++i) {
      
      // Saves ticket data from server into vars
      var search_ticket_title = results[i].Event;
      var search_ticket_date = results[i].EventDate;
      var search_ticket_type = results[i].TicketType;
      var search_ticket_price = results[i].Price;
      var search_ticket_seat = results[i].Seat;
      var search_ticket_id = results[i].ID;
      var search_ticket_seller = results[i].Seller;

      console.log("Title: " + search_ticket_title);
      console.log("Date: " + search_ticket_date);
      console.log("Type: " + search_ticket_type);
      console.log("Price: " + search_ticket_price);
      console.log("Seat: " + search_ticket_seat);
      console.log("ID: " + search_ticket_id);
      console.log("Seller: " + search_ticket_seller);

      // Format the data grabbed from the tickets array
      var searchappendText = '<div class="ticket_div">';
      searchappendText += '<div class="date">';
      searchappendText += '<p>Event Date</p>';
      searchappendText += '<h5>' + search_ticket_date.substring(5, 10) + '</h5>';
      searchappendText += '</div>';
      searchappendText += '<div class="ticket_description">'
      searchappendText += '<p class="ticket_category">' + search_ticket_type + '</p>';
      searchappendText += '<p class="ticket_specifics"><a #="' + search_ticket_id + '" href="ticket_bio.html' + '?id=' + search_ticket_id + '">' + search_ticket_title + '</a></p>';
      // if (localStorage.username == search_ticket_seller) {
      //   searchappendText += '<div class="ticket_status">Your Ticket</div>';
      // }
      // else {
      //   console.log("NOT SELLER");
      //   console.log(localStorage.username + " " + search_ticket_id);
      //   socket.emit('see_my_specific_bid_ticket', localStorage.username, search_ticket_id);
      //   console.log('Specific Bid Ticket Worked');
      //   socket.on('personal_list_of_bids', function(uniqueTicketObj) {
      //     console.log(uniqueTicketObj[0].Buyer);
      //     var search_ticket_buyer = uniqueTicketObj[0].Buyer;
      //     var search_specific_id = uniqueTicketObj[0].ID;
      //     if (localStorage.username == search_ticket_buyer && search_ticket_id == search_specific_id) {
      //       searchappendText += '<div class="ticket_status">Bidding</div>';
      //       console.log("IN THE IF CLAUSE");
      //     }
      //   });
      // }
     
      // searchappendText += '<p class="ticket_price">$' + search_ticket_price + '</p>';
      // searchappendText += '</div>';
      // searchappendText += '</div>';
      // console.log(searchappendText);

      // For each ticket logged, append it to the .all_tickets div
      $(".all_tickets").append(searchappendText);
    }
  });
});

  



