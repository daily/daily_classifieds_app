var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send('This is the sent message - you\'re at the server page');
});

var server = app.listen(8000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Testing socket', host, port);
});

var io = require('socket.io')(server);

io.on('connection', function (socket){
	console.log("Connected to client");
})